const express = require("express")
const http = require("http")
const WebSocket = require("ws")
const cors = require("cors")
const { json } = require("express")
const app = express()
const PORT = 9898;
const HOST = '0.0.0.0';

require('./src/Routes/index')(app)

app.use(cors())
app.use(express.json())

const server = http.createServer(app)
require('./webSocketServer')(server)

// server.on('listening',function(){
//     console.log('ok, server is running');
// });

// server.listen(9898);

server.listen(PORT, HOST, () => {
    console.log("Servidor conectado na porta: " + server.address().port)
})