const WebSocket = require("ws")
const EventController = require('./src/Controllers/EventController');
var eventController

module.exports = (server) => {
    
    const wss = new WebSocket.Server({ server })
    

    wss.on("connection", (ws) => {
        ws.on("message", (message) => {
            eventController = new EventController(message)
            eventController.prepare()
            eventController.countIndex()
            if (eventController.countPositions == -1) {
                ws.send(JSON.stringify(getError()));
            } else {
                var index = 0
                var intervalFunction = setInterval(function() {
                    if (index <= eventController.countPositions) {
                        const result = eventController.getEventByIndex(index)
                        ws.send(JSON.stringify(result));
                        index++
                    } else {
                        clearInterval(this)
                        ws.send(JSON.stringify(getFinish()));
                    }
                }, 5000);
                
                if (index <= eventController.countPositions) {
                    intervalFunction
                } else {
                    clearInterval(intervalFunction)
                }
            }
        })
    })
}

function getError() {
    return {error: "Erro generico..."};
}

function getFinish() {
    return {finish: true};
}