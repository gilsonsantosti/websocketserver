const RoundController = require('../Controllers/RoundController');

module.exports = (app) => {
    app.get("/rounds", RoundController.get);
}