
module.exports = class EventController {
    constructor(id) {
        this.id = id
        this.eventData = new EventData(id)
        this.countPositions = 0
    }
    prepare() {
        this.eventData.getJson()
        if (this.eventData.indexCount != -1) {
            this.eventData.sortedList()
            console.log('sucesso list === ' + this.eventData.listEvent)
        }
    }

    countIndex() {
        this.countPositions = this.eventData.indexCount
        console.log('sucesso index === ' + this.countPositions)
    }

    getEventByIndex(index) {
        return this.eventData.getEventByIndex(index)
    }
    
}

class EventData {

    constructor(id, listEvent, indexCount)  {
        this.id = id
        this.listEvent = listEvent
        this.indexCount = indexCount
    }

     getJson() {
        try {
            const json = require('../Data/Events/' + this.id + '.json');
            this.listEvent = json;
            console.log('length === ' + this.listEvent.api.events.length);
            this.indexCount = this.listEvent.api.events.length - 1;
        } catch (error) {
            console.log('erro no arquivo....');
            this.indexCount = -1;
        }
    }

     getEventByIndex(index) {
        if (index <= this.indexCount) {
            return this.listEvent[index];
        }
    }
    
     sortedList(){
        this.listEvent = this.listEvent.api.events.sort((a, b) => {
            return a.elapsed < b.elapsed
        });
    }
    
}