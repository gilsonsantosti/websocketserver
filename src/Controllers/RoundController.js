const json = require('../Data/round.json')

exports.get = (req, res, next) => {
    res.status(200).send(json);
}

function getJson () {
    return JSON.parse(json);
}